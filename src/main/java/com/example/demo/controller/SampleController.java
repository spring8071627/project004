package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.dto.SampleDTO;

/*
 * 리턴 타입이 void인 경우,url 경로와 일치하는 html 파일을 찾아 반환
 * 파일경로: templates/ + sample/ + ex.html
 * */

@Controller
@RequestMapping("/sample")
public class SampleController {

    @GetMapping("/ex1")
    public void ex1() {
    }

    @GetMapping("/ex2")
    public void ex2(Model model) {
    	model.addAttribute("msg","aaa");
    }
    
    @GetMapping("/ex3")
    public void ex3(Model model) {
    	SampleDTO semplDto = new SampleDTO(1,"aaa",LocalDateTime.now());
    	 model.addAttribute("dto", semplDto);
    }
    
    @GetMapping("/ex4")
    public void ex4(Model model) {
    	List<SampleDTO> list = new ArrayList<>();
    	list.add(new SampleDTO(1,"aaa",LocalDateTime.now()));
    	list.add(new SampleDTO(2,"bbb",LocalDateTime.now()));
    	list.add(new SampleDTO(3,"ccc",LocalDateTime.now()));
    	model.addAttribute("list",list);
    }
    
    @GetMapping("/ex5")
    public void ex5(Model model) {
    	SampleDTO sampleDTO = new SampleDTO(1,"aaa",LocalDateTime.now());
    	model.addAttribute("result", "success");
    	model.addAttribute("dto",sampleDTO);
    }

  @GetMapping("/ex6")
  public void ex6(Model model) {
	  SampleDTO sampleDTO = new SampleDTO(1, "aBC", LocalDateTime.now());
	  model.addAttribute("dto",sampleDTO);
  }
  // 템플릿 레이아웃1
  @GetMapping("exLayout1")
  public void ex7() { // 레이아웃을 반환
	  
	  }
  @GetMapping("/exLayout1-2")
  public void ex8() {
	  
  }
  @GetMapping("/fragment2")
  public void ex9() { // 조각페이지를 반환
	  
  }
  
  @GetMapping("/fragment3")
  public void ex10() {
	  
  }
  
  @GetMapping("/fragment4")
  public void ex11() {
	  
  }
  @GetMapping("/fragment5")
  public void ex12() {
	  
  }
  @GetMapping("/fragment6")
  public void ex13() {
	  
  }
}
